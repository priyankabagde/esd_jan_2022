const express = require("express");
const router = express.Router();
router
  .route("/")
  .get((req, res) => {
    res.setHeader("Content-Type", "text/html");
    res.send("student listing");
  })
  .post((req, res) => {
    res.setHeader("Content-Type", "text/html");
    res.send("student post");
  })
  .put((req, res) => {
    res.setHeader("Content-Type", "text/html");
    res.send("student put");
  })
  .delete((req, res) => {
    res.setHeader("Content-Type", "text/html");
    res.send("student delete");
  });

module.exports = router;
