var express = require("express");
var app = express();
var student = require("./students");
var home = require("./home");
var library = require("./library");

app.use("/student", student);
app.use("/", home);
app.use("/library", library);

app.use(function (req, res, next) {
  res.status(404);
  res.send("404: File Not Found. Please check the url");
});

app.listen(8989, () => {
  console.log("Listening at port 8989");
});
