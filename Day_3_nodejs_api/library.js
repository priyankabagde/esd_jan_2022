const express = require("express");
const router = express.Router();

router
  .route("/")
  .get((req, res) => {
    res.setHeader("Content-Type", "text/html");
    res.send("library listing");
  })
  .post((req, res) => {
    res.setHeader("Content-Type", "text/html");
    res.send("library post");
  })
  .put((req, res) => {
    res.setHeader("Content-Type", "text/html");
    res.send("library put");
  })
  .delete((req, res) => {
    res.setHeader("Content-Type", "text/html");
    res.send("library delete");
  });

module.exports = router;
