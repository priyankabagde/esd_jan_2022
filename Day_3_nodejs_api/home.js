const express = require("express");
const router = express.Router();
router
  .route("/")
  .get((req, res) => {
    res.setHeader("Content-Type", "text/html");
    res.send("home page hit");
  })
  .post((req, res) => {
    res.setHeader("Content-Type", "text/html");
    res.send("home post");
  })
  .put((req, res) => {
    res.setHeader("Content-Type", "text/html");
    res.send("home put");
  })
  .delete((req, res) => {
    res.setHeader("Content-Type", "text/html");
    res.send("home delete");
  });

module.exports = router;
