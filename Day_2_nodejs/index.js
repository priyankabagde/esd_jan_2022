var mymath = require("./mymodule");
var geo = require("./geo");
var http = require("http");

http.createServer((req, res) => {
    // res.writeHead(200, {'Content-Type':"text/plain"});
    // res.writeHead('Content-Type', 'application/json');
    res.writeHead(200, { "Content-Type": "text/html" });
    res.write("this is line 1");

    console.log(req.url);

    if (req.url == "/") {
      res.write("</br>Home req hit");
    } else if (req.url == "/teacher") {
      res.write("Teacher req hit</br>");
    } else if (req.url == "/student") {
      res.write("Student req hit</br>");
    } else {
      res.end("Page Not Found....");
    }

    // res.end("hello world....");
  })
  
  .listen(8080, () => {
    console.log("listening at port 8080…");
  });

// console.log('hello class');

// area = new geo.Rectangle(10,3);
// console.log(area.area());

// console.log(mymath.add(5,6));
// console.log(mymath.add(3,6));
// console.log(mymath.sub(6,3));

// objbank = new mymath.Bank(5000);
// objbank.showBal();
