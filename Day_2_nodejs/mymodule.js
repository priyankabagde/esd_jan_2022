exports.add = (x,y) =>{
    return x + y;
}

exports.sub = (x,y) => {
    return x - y;
}

exports.Bank = class {
    constructor(b){
        this.bal = b;
    }

    showBal(){
        console.log(`Balance: ${this.bal}`);
    }
}

// make a class rectangle, l,b, area(); 
// in a new module geo.js