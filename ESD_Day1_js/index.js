import Bank from './Bank.js';
import HDFCBank from './HDFCBank.js';

var cobj = new Bank(5000);
console.log(cobj);
cobj.showBalance();


var hobj = new HDFCBank(1000);
hobj.showBalance();
hobj.deposit(100);
hobj.showBalance();